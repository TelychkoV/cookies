import {
  firstLayerMarkUp,
  firstLayerButtons,

} from "../constants/first_page_constants";

import {
  firstBlock,
  secondBlock,
  thirdBlock,
  fourthBlock,
  settingsNavigationLayout

} from "../constants/second_page_constants";
import '../styles/index.scss';

const modalBlock = document.querySelector('.modal');
const modalContentFirst = document.querySelector('.modal__content');
const settingsBlock = document.querySelector('.modal-settings');
const modalNavigation = document.querySelector('.modal__navigation');
const firstBlockElem = document.getElementById('firstBlock');
const secondBLockElem = document.getElementById('secondBLock');
const thirdBLockElem = document.getElementById('thirdBLock');
const fourthBLockElem = document.getElementById('fourthBLock');
const modalSettingsBtns = document.querySelector('.modal-settings__navigation');

// Checkboxes default status
// let firstCheckBoxStatus = false;
// let secondCheckBoxStatus = false;

modalContentFirst.innerHTML += firstLayerMarkUp;
modalNavigation.innerHTML = firstLayerButtons;
firstBlockElem.innerHTML = firstBlock;
secondBLockElem.innerHTML = secondBlock;
thirdBLockElem.innerHTML = thirdBlock;
fourthBLockElem.innerHTML = fourthBlock;
modalSettingsBtns.innerHTML = settingsNavigationLayout;

const manageBtn = document.getElementById('manage-btn');
const acceptBtn = document.getElementById('accept-btn');
const doneBtn = document.getElementById('done-Btn');
const rejectBtn = document.getElementById('reject-Btn');
const firstCheckBox = document.getElementById('first-ch');
const secondCheckBox = document.getElementById('second-ch');
const checkboxLabelOne = document.getElementById('checkboxLabelOne');
const checkboxLabelTwo = document.getElementById('checkboxLabelTwo');

const toggleModalPage = () => {
  modalBlock.classList.add('modal_hidden');
  settingsBlock.classList.add('modal-settings_visible');
};


const handleManageBtnClick = () => {
  toggleModalPage();
}

const handleEnterClick = (e) => {
  if (e.keyCode === 13) {
    toggleModalPage();
  }
}

const handleAcceptClick = () => {
  modalBlock.classList.add('modal_hidden');
  document.cookie = 'necessary=true;';
  document.cookie = 'functional=true';
  document.cookie = 'performance=true';
}

const handleDoneClick = () => {
  settingsBlock.classList.remove('modal-settings_visible');
  document.cookie = 'necessary=true;';
  document.cookie = `functional=${firstCheckBox.checked}`;
  document.cookie = `performance=${secondCheckBox.checked}`;
}

// Methods to manipulate checkboxes states
const handleRejectClick = () => {
  firstCheckBox.checked = false;
  secondCheckBox.checked = false;
  checkboxLabelOne.innerText = 'Off';
  checkboxLabelTwo.innerText = 'Off';
}

const handleFirstCheckboxClick = () => {
  if (!firstCheckBox.checked) {
    checkboxLabelOne.innerText = 'Off';
  } else {
    checkboxLabelOne.innerText = 'On';
  }
}

const handlesecondCheckBoxClick = () => {
  if (!secondCheckBox.checked) {
    checkboxLabelTwo.innerText = 'Off';
  } else {
    checkboxLabelTwo.innerText = 'On';
  }
}
manageBtn.addEventListener('click', handleManageBtnClick);
manageBtn.addEventListener('keyup', handleEnterClick);
acceptBtn.addEventListener('click', handleAcceptClick);
doneBtn.addEventListener('click', handleDoneClick);
rejectBtn.addEventListener('click', handleRejectClick);
firstCheckBox.addEventListener('click', handleFirstCheckboxClick);
secondCheckBox.addEventListener('click', handlesecondCheckBoxClick);